import re

map_of_spelled_out_to_integer = {
        "one": 1,
        "two": 2,
        "three":3,
        "four":4,
        "five":5,
        "six":6,
        "seven":7,
        "eight":8,
        "nine":9,
        }

def convert_to_integer(spelled_out):
    try:
        return(int(spelled_out))
    except:
        return map_of_spelled_out_to_integer.get(spelled_out)

def all_digits(a_line):
    return re.findall(r'(?=(one|two|three|four|five|six|seven|eight|nine|\d))', a_line)

def first(a_list):
    return a_list[0]

def last(a_list):
    return a_list[-1]

def first_digit(a_line):
    return convert_to_integer(first(all_digits(a_line)))

def last_digit(a_line):
    return convert_to_integer(last(all_digits(a_line)))

def two_digit_number(first_digit, last_digit):
    return int(f"{first_digit}{last_digit}")

def calibration_value(a_line):
    return two_digit_number(first_digit(a_line),last_digit(a_line))

def all_lines(file_document):
    with open(file_document) as f:
        for a_line in f:
            yield a_line

def all_calibration_values(document):
    return (calibration_value(line) for line in all_lines(document))

print(sum(all_calibration_values("input")))
