# Snow Island
import re
from collections import namedtuple

def all_lines(file_document):
    with open(file_document) as f:
        for a_line in f:
            yield a_line


def games_id(game):
    pattern = re.compile("^Game (?P<game_id>\d+):")
    return int(pattern.match(game).group("game_id"))
   
def revealed_sets(game):
    return [ _.strip() for _ in game.split(":")[1].split(";")]

def red_in(a_set):
    pattern = re.compile("(?P<red>\d+) red")
    if pattern.search(a_set):
        return int(pattern.search(a_set).group("red"))
    else:
        return 0
def blue_in(a_set):
    pattern = re.compile("(?P<blue>\d+) blue")
    if pattern.search(a_set):
        return int(pattern.search(a_set).group("blue"))
    else:
        return 0
def green_in(a_set):
    pattern = re.compile("(?P<green>\d+) green")
    if pattern.search(a_set):
        return int(pattern.search(a_set).group("green"))
    else:
        return 0

def valid_set(a_set, cube_set):
    return red_in(a_set) <= cube_set.red and blue_in(a_set) <= cube_set.blue and green_in(a_set) <= cube_set.green



def has_valid_set(game):
    all_test = [valid_set(a_set, cube_set) for a_set in revealed_sets(game)]
    return all(all_test)

def get_valid_game_ids(games):
    for game in games:
        if has_valid_set(game):
            yield games_id(game)

games = all_lines("input")
Cube = namedtuple("Cube",["red","green","blue"])
cube_set = Cube(
        12,
        13,
        14,
        )
print(sum(get_valid_game_ids(games)))

def cube(game):
    red = max([red_in(_) for _ in revealed_sets(game)])
    blue = max([blue_in(_) for _ in revealed_sets(game)])
    green = max([green_in(_) for _ in revealed_sets(game)])
    return (red,green,blue)

def power(cube):
        return cube[0]*cube[1]*cube[2]

def powers(cubes):
    for cube in cubes:
        yield power(cube)

def cubes(games):
    for game in games:
        yield cube(game)

games = all_lines("input")
print(sum(powers(cubes(games))))
